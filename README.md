# ActivityPub HTTP Signatures

A library for creating, parsing, and verifying HTTP signature headers, as per the [Signing HTTP Messages draft 08](https://datatracker.ietf.org/doc/html/draft-cavage-http-signatures-08) specification.

## Basic use

### Signing

The following example illustrates using the library to sign requests:

```javascript
import { sign } from 'activitypub-http-signatures';
import fetch from 'node-fetch';

// You should make the public key available at this URL
const publicKeyId = 'http://my-site.example/@paul#main-key';
const privateKey = `-----BEGIN RSA PRIVATE KEY-----
${process.env.PRIVATE_KEY}
-----END RSA PRIVATE KEY-----`;

export function getResource(url) {
    // Only add the headers you want to sign.
    // This is just an example, as the host and date
    // properties will be added to your headers object
    // if they're missing, with the following default values:
    const headers = {
        host: new URL(url).host,
        date: new Date().toUTCString()
    }

    // Generate the signature
    const signature = sign(
        { publicKeyId, privateKey },
        {
            url,
            method: 'GET',
            headers
        }
    );

    // Create the HTTP request with the generated header
    return fetch(
        url,
        headers: {
            ...headers,
            signature,
            accept: 'application/ld+json'
        }
    )
}
```

### Verifying

```javascript
import { parse, verify } from 'activitypub-http-signatures';
import fetch from 'node-fetch';

export async function verifyIncomingRequestSignature(req){
    // Parse the fields from the signature header
    const { headers, signature, keyId } = parse(req.headers.signature);

    // Get the public key object using the provided key
    const keyRes = await fetch(keyId, { headers: { accept: 'application/ld+json, application/json' } });
    const { publicKey } = await keyRes.json();

    // Verify the signature
    const success = verify(
        signature,	// The signature from the signature header, as a Buffer object
        publicKey.publicKeyPem,	// The PEM string from the public key object
        {
          target: req.url,		// The pathname of the incoming request
          method: req.method,	// The method of the incoming request
          headers: req.headers	// The headers dict from the incoming request
        },
        headers // The list of headers actually used in signing the request, from the signature header
    );

    if(!success){
        throw new Error('http signature validation failed')
    } else {
        return publicKey.owner
    }
}
```


## Dependencies
None
<a name="module_activitypub-http-signatures"></a>

## activitypub-http-signatures
Activitypub HTTP Signatures
Based on [HTTP Signatures draft 08](https://datatracker.ietf.org/doc/html/draft-cavage-http-signatures-08)


* [activitypub-http-signatures](#module_activitypub-http-signatures)
    * [.getSignString(options)](#module_activitypub-http-signatures.getSignString) ⇒ <code>string</code>
    * [.sign(keyOptions, reqOptions)](#module_activitypub-http-signatures.sign) ⇒ <code>string</code>
    * [.signSha256(privateKey, stringToSign)](#module_activitypub-http-signatures.signSha256) ⇒ <code>Buffer</code>
    * [.parse(signature)](#module_activitypub-http-signatures.parse) ⇒ <code>object</code>
    * [.verify(signature, publicKey, request, headerNames)](#module_activitypub-http-signatures.verify) ⇒ <code>boolean</code>
    * [.verifySha256(signature, key, signedString)](#module_activitypub-http-signatures.verifySha256) ⇒ <code>boolean</code>

<a name="module_activitypub-http-signatures.getSignString"></a>

### activitypub-http-signatures.getSignString(options) ⇒ <code>string</code>
Generate the string to be signed for the signature header

**Kind**: static method of [<code>activitypub-http-signatures</code>](#module_activitypub-http-signatures)  

| Param | Type | Description |
| --- | --- | --- |
| options | <code>Object</code> | Options |
| options.target | <code>string</code> | The pathname of the request URL (including query and hash strings) |
| options.method | <code>string</code> | The HTTP request method |
| options.headers | <code>object</code> | Object whose keys are http header names and whose values are those headers' values |

<a name="module_activitypub-http-signatures.sign"></a>

### activitypub-http-signatures.sign(keyOptions, reqOptions) ⇒ <code>string</code>
Sign a requst and return signature header
Make sure the headers object only contains the headers you wish to sign.
If absent, the `date` and `host` header values will be added to this object.

**Kind**: static method of [<code>activitypub-http-signatures</code>](#module_activitypub-http-signatures)  
**Returns**: <code>string</code> - Value for the signature header  

| Param | Type | Description |
| --- | --- | --- |
| keyOptions | <code>object</code> | Key options |
| keyOptions.publicKeyId | <code>string</code> | URI for public key |
| keyOptions.privateKey | <code>string</code> | Private key |
| reqOptions | <code>object</code> | Request options |
| reqOptions.url | <code>string</code> | URL of the request to sign |
| reqOptions.method | <code>string</code> | Method of the request |
| reqOptions.headers | <code>object</code> | Object whose keys are header names and values are those headers' values |

<a name="module_activitypub-http-signatures.signSha256"></a>

### activitypub-http-signatures.signSha256(privateKey, stringToSign) ⇒ <code>Buffer</code>
Sign a string with a private key using sha256 alg

**Kind**: static method of [<code>activitypub-http-signatures</code>](#module_activitypub-http-signatures)  
**Returns**: <code>Buffer</code> - Signature buffer  

| Param | Type | Description |
| --- | --- | --- |
| privateKey | <code>string</code> | Private key |
| stringToSign | <code>string</code> | String to sign |

<a name="module_activitypub-http-signatures.parse"></a>

### activitypub-http-signatures.parse(signature) ⇒ <code>object</code>
Parse the signature header.
The returned object will contain header names array,
and signature as a buffer object, as well as
any other fields as strings (e.g. keyId and algorithm)

**Kind**: static method of [<code>activitypub-http-signatures</code>](#module_activitypub-http-signatures)  
**Returns**: <code>object</code> - Map of signature field keys to values  

| Param | Type | Description |
| --- | --- | --- |
| signature | <code>string</code> | Signature header value |

<a name="module_activitypub-http-signatures.verify"></a>

### activitypub-http-signatures.verify(signature, publicKey, request, headerNames) ⇒ <code>boolean</code>
Verify a signature against a key and request

**Kind**: static method of [<code>activitypub-http-signatures</code>](#module_activitypub-http-signatures)  
**Returns**: <code>boolean</code> - True if the request was verified  

| Param | Type | Description |
| --- | --- | --- |
| signature | <code>Buffer</code> | The signature to verify, as a buffer |
| publicKey | <code>string</code> | The public key to use to verify the signature |
| request | <code>object</code> | The request properties to compare against |
| request.target | <code>string</code> | The URL pathname of the request |
| request.method | <code>string</code> | The http method of the request |
| request.headers | <code>object</code> | Dict of the HTTP headers from the request |
| headerNames | <code>array</code> | The names of the headers actually used to create the signature, in order |

<a name="module_activitypub-http-signatures.verifySha256"></a>

### activitypub-http-signatures.verifySha256(signature, key, signedString) ⇒ <code>boolean</code>
Verify the signature of a string using SHA256

**Kind**: static method of [<code>activitypub-http-signatures</code>](#module_activitypub-http-signatures)  
**Returns**: <code>boolean</code> - True if the signature was verified  

| Param | Type | Description |
| --- | --- | --- |
| signature | <code>Buffer</code> | The signature to verify, as a buffer |
| key | <code>string</code> | The public key to use to verify the signature |
| signedString | <code>string</code> | The original string to verify against |

