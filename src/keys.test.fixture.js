export default {
	private: `-----BEGIN RSA PRIVATE KEY-----
MIIBOwIBAAJBAKC9U+CuSjPm/nX9XZ+9S1JDHHCYJWpks9qJ//GLdtrmOWlhdc3C
ED7/97c12Fa6ZTnii/nLUzmz5Z5kMQ5CLJUCAwEAAQJBAJG3kRaaakJrIjusmPd7
D5FfraSVCTZOXI29lP1QRUtjAtJaPDD058HOSJM49xmbGDPaTUlu01B1nA8rcByV
3fECIQDMhNCDrsV6g0Utd9gPSaa4lHKShgRt0mO8NOHDhlvpswIhAMkzYULfiawX
6fiEwAkNjMxY1OsUNIdEFGL1WAgMNVyXAiBfa6QhnDxU4cQ3549t6o4X1mLyTAbq
+ltAJ2giIqDlkwIgMYXPA8nHti8wrLXoGpJWPJoE1lPj1gOAzRa8c0al/8MCIQCh
WS/vhJsQmetk9fTzNeT6I+LIwpkxkerpXoyebUNMdg==
-----END RSA PRIVATE KEY-----`,
	public: `-----BEGIN PUBLIC KEY-----
MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAKC9U+CuSjPm/nX9XZ+9S1JDHHCYJWpk
s9qJ//GLdtrmOWlhdc3CED7/97c12Fa6ZTnii/nLUzmz5Z5kMQ5CLJUCAwEAAQ==
-----END PUBLIC KEY-----`,
	private2: `-----BEGIN RSA PRIVATE KEY-----
MIIBOgIBAAJBAMcnJvc0ZPUWAE+Z1XK6DtQ6k/xXtgVyWpb1FbSEAkah3g4AkPjo
3HLi8bPrl4OczZPtnGQdop+cDY9CDLl+zRcCAwEAAQJAJECyTSnSfvLkV5kvS9Um
wedv7O2iitdIOlae/t9Q8r5OTK6gkMWd/abefA5iClvCVr856rNhLYxke8XcV2NR
wQIhAOdpkPy8Ihz5XQIFHpBLw0gg5d+8sTGFxvQkNd3EEvsRAiEA3FAgf8dk93RE
1AY/RW6SvHVFb/Z96WCcnqwEPcf6tacCIQCxq5Z6xML4NehX3bBanyCep0t2nTv6
Rri6x3Zn/tuH0QIgVUMBQNJXBdlOZcvBIsrHKP4tkEYRtob4NgQWZB6HrVMCIBxv
qQ8fsof+doWd6IfZh8aFkjw5YeRD3eHYIx1r66nn
-----END RSA PRIVATE KEY-----`,
	public2: `-----BEGIN PUBLIC KEY-----
MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAMcnJvc0ZPUWAE+Z1XK6DtQ6k/xXtgVy
Wpb1FbSEAkah3g4AkPjo3HLi8bPrl4OczZPtnGQdop+cDY9CDLl+zRcCAwEAAQ==
-----END PUBLIC KEY-----`
};