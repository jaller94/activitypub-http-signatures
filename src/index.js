/**
 * Activitypub HTTP Signatures
 * Based on [HTTP Signatures draft 08](https://datatracker.ietf.org/doc/html/draft-cavage-http-signatures-08)
 * @module activitypub-http-signatures
 */

import crypto from 'crypto';


// token definitions from definitions in rfc7230 and rfc7235
const token	= String.raw`[!#$%&'\*+\-\.\^_\`\|~0-9a-z]+`;	// Key or value
const qdtext	= String.raw`[^"\\\x7F]`;	// Characters that don't need escaping
const quotedPair	= String.raw`\\[\t \x21-\x7E\x80-\xFF]`;	// Escaped characters
const quotedString	= `(?:${qdtext}|${quotedPair})*`;
const fieldMatch	= new RegExp(String.raw`(?<=^|,\s*)(${token})\s*=\s*(?:(${token})|"(${quotedString})")(?=,|$)`, 'ig');
const parseSigFields	= str => Object.fromEntries(
	Array.from(
		str.matchAll(fieldMatch)
	).map(
		// capture groups: 1=fieldname, 2=field value if unquoted, 3=field value if quoted
		v=>[
			v[1],
			v[2] ?? v[3].replace(
				/\\./g,
				c=>c[1]
			)
		]
	)
);

const defaultHeaderNames = headers => ['(request-target)', ...Object.keys(headers)];

/**
 * Generate the string to be signed for the signature header
 * @param {Object}	options	Options
 * @param {string}	options.target	The pathname of the request URL (including query and hash strings)
 * @param {string}	options.method	The HTTP request method
 * @param {object}	options.headers	Object whose keys are http header names and whose values are those headers' values
 * @param {string[]=}	headerNames	The names of the headers actually used to create the signature, in order
 * @returns {string}
 */
export function getSignString({ target, method, headers }, headerNames = defaultHeaderNames(headers)) {
	const requestTarget = `${method.toLowerCase()} ${target}`;
	headers = {
		...headers,
		'(request-target)': requestTarget
	};
	return headerNames.map(header => `${header.toLowerCase()}: ${headers[header]}`).join('\n');
}

/**
 * Sign a request and return signature header
 * Make sure the headers object only contains the headers you wish to sign.
 * If absent, the `date` and `host` header values will be added to this object.
 * @param {object}	keyOptions	Key options
 * @param {string}	keyOptions.publicKeyId	URI for public key
 * @param {string}	keyOptions.privateKey	Private key
 * @param {object}	reqOptions	Request options
 * @param {string}	reqOptions.url	URL of the request to sign
 * @param {string}	reqOptions.method	Method of the request
 * @param {object}	reqOptions.headers	Object whose keys are header names and values are those headers' values
 * @param {string[]=}	headerNames	The names of the headers actually used to create the signature, in order
 * @returns {string} Value for the signature header
 */
export function sign({ publicKeyId, privateKey }, { url, method, headers }, headerNames){
	const { host, pathname, search, hash } = new URL(url);
	const target = `${pathname}${search}${hash}`;
	headers.date ??= new Date().toUTCString();
	headers.host ??= host;

	headerNames ??= defaultHeaderNames(headers);

	const stringToSign = getSignString({ target, method, headers }, headerNames);

	const signature = signSha256(privateKey, stringToSign).toString('base64');

	return `keyId="${publicKeyId}",headers="${headerNames.join(' ')}",signature="${signature.replace(/"/g, '\\"')}",algorithm="rsa-sha256"`;
}

/**
 * Sign a string with a private key using sha256 alg
 * @param {string} privateKey Private key
 * @param {string} stringToSign String to sign
 * @returns {Buffer} Signature buffer
 */
export function signSha256(privateKey, stringToSign) {
	const signer = crypto.createSign('sha256');
	signer.update(stringToSign);
	const signature = signer.sign(privateKey);
	signer.end();
	return signature;
}

/**
 * Parse the signature header.
 * The returned object will contain header names array,
 * and signature as a buffer object, as well as
 * any other fields as strings (e.g. keyId and algorithm)
 * @param {string} signature Signature header value
 * @returns {object} Map of signature field keys to values
 */
export function parse(signature){
	const fields = parseSigFields(signature);

	fields.headers = (fields.headers ?? 'date').split(/\s+/);
	fields.signature = Buffer.from(fields.signature, 'base64');
	fields.algorithm ??= 'rsa-sha256';

	return fields;
}

/**
 * Verify a signature against a key and request
 * @param	{Buffer}	signature	The signature to verify, as a buffer
 * @param	{string}	publicKey	The public key to use to verify the signature
 * @param	{object}	request	The request properties to compare against
 * @param	{string}	request.target	The URL pathname of the request
 * @param	{string}	request.method	The http method of the request
 * @param	{object}	request.headers	Dict of the HTTP headers from the request
 * @param	{string[]=}	headerNames	The names of the headers actually used to create the signature, in order
 * @returns	{boolean}	True if the request was verified
 */
export function verify(signature, key, { target, method, headers }, headerNames) {
	const signString = getSignString({ target, method, headers }, headerNames);
	return verifySha256(signature, key, signString);
}

/**
 * Verify the signature of a string using SHA256
 * @param {Buffer} signature	The signature to verify, as a buffer
 * @param {string} key	The public key to use to verify the signature
 * @param {string} signedString	The original string to verify against
 * @returns {boolean} True if the signature was verified
 */
export function verifySha256(signature, key, signedString){
	const verifier = crypto.createVerify('sha256');
	verifier.write(signedString);
	verifier.end();

	return verifier.verify(key, signature);
}
