import { expect, test } from '@jest/globals';
import { sign, signSha256, verify, verifySha256, parse } from './index.js';
import keys from './keys.test.fixture.js';

test('sign & verify', () => {
	const signed = signSha256(keys.private, 'test');
	expect(verifySha256(signed, keys.public, 'test')).toBe(true);
});

test('sign', ()=>{
	const publicKeyId = 'http://key.example/#id';
	const url = 'http://example.com/a';
	const method = 'GET';
	const headers = {
		date: new Date('2021').toUTCString()
	};

	expect(sign(
		{ publicKeyId, privateKey: keys.private },
		{ url, method, headers }
	)).toMatchSnapshot();
});

test('verify', ()=>{
	const method = 'GET';
	const headers = {
		date: new Date('2021').toUTCString(),
		host: 'example.com',
		other: 'ignored'
	};

	const signature = Buffer.from('Cu8UDaP7kc/FNJPrkjEosMl6opF0BB9aK0IgEC3yHGjAgJUZawt7s8laVGVv+reRF2PVB6fcREzJTBZXDfA3NA==', 'base64');
	const headerNames = ['(request-target)', 'date', 'host'];

	expect(verify(signature, keys.public, {
		target: '/a',
		method,
		headers
	}, headerNames)).toBe(true);

	// Deprecated:
	delete headers.other;
	expect(verify(signature, keys.public, {
		target: '/a',
		method,
		headers
	})).toBe(true);
});

test('parse', ()=>{
	expect(parse('keyId="http://key.example/#id",signature="Cu8UDaP7kc/FNJPrkjEosMl6opF0BB9aK0IgEC3yHGjAgJUZawt7s8laVGVv+reRF2PVB6fcREzJTBZXDfA3NA=\\="')).toMatchSnapshot();
});